
public class MatrixMul {

	public static void matmul(int m, int n, int p, int arr1[][], int arr2[][]) {
		int resarr[][] = new int[m][p];
		
		for (int i=0;i<m;i++) {
			for (int j=0;j<n;j++) {
				System.out.print(arr1[i][j]+"  ");
			}
			System.out.println();
		}
		System.out.println();
		for (int i=0;i<n;i++) {
			for (int j=0;j<p;j++) {
				System.out.print(arr2[i][j]+"  ");
			}
			System.out.println();
		}
		System.out.println();
		
		for (int i=0;i<m;i++) {
			for (int j=0;j<p;j++) {
				resarr[i][j]=0;
				for (int k=0;k<n;k++) {
					resarr[i][j] += (arr1[i][k]*arr2[k][j]);
				}
				System.out.print(resarr[i][j]+"  ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {

//		Logic Source: https://en.wikipedia.org/wiki/Matrix_multiplication

		int m = 4;
		int n = 4;
		int p = 4;
		
//		int arr1[][] = new int[m][n];
//		int arr2[][] = new int[n][p];
		int arr1a[][]= {{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}};
		int arr1b[][]= {{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4}};

		int arr2[][]= {{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4}};

		matmul(m,n,p,arr1a,arr2);
		System.out.println();
		System.out.println();
		matmul(m,n,p,arr1b,arr2);

	}
}
