
interface Trans {
	String AuthMessageTransId="0100";
	String AdviceMessageTransId="0120";
	String ClearMessageTransId="1440";
	abstract void auth(int refnum);
	abstract void advice(int refnum);
	abstract void clear(int  refnum);
}


abstract class ToAcqHidden implements Trans {
	public void auth(int refnum) {
		if(refnum%2==0) {
			System.out.println(AuthMessageTransId+" Transaction is Approved for the refnum " + refnum + ". Enjoy!!!");
		} else {
			System.out.println(AuthMessageTransId+" Transaction is Declined for the refnum " + refnum + ". Sorry!!!");
		}
	}
}

class ToAcq extends ToAcqHidden{
	public void advice(int refnum) {
		System.out.println(AdviceMessageTransId+" Advice Messages can be sent only to Issuers and not to Acquirers!!!");
	}
	public void clear(int refnum) {
		System.out.println(ClearMessageTransId+" Clearing Messages can be sent only to PCI Gateway and not to Acquirers!!!");
	}
	
}


abstract class ToIssHidden implements Trans {
	public void advice(int refnum) {
		if(refnum%2==0) {
			System.out.println(AdviceMessageTransId+" Transaction is Approved by PCI Gateway for the refnum " + refnum + ". Enjoy!!!");
		} else {
			System.out.println(AdviceMessageTransId+" Transaction is Declined by PCI Gateway for the refnum " + refnum + ". Sorry!!!");
		}
	}
}

class ToIss extends ToIssHidden{
	public void auth(int refnum) {
		System.out.println(AuthMessageTransId+" Authorization Messages can be sent only to Acquirers and not to Issuers!!!");
	}
	public void clear(int refnum) {
		System.out.println(ClearMessageTransId+" Clearing Messages can be sent only to PCI Gateway and not to Issuers!!!");
	}
	
}


abstract class ToPCIHidden implements Trans {
	public void clear(int refnum) {
		if(refnum%2==0) {
			System.out.println(ClearMessageTransId+" Transaction is Cleared by PCI Gateway for the refnum " + refnum + ". Enjoy!!!");
		} else {
			System.out.println(ClearMessageTransId+" Transaction is not Cleared by PCI Gateway for the refnum " + refnum + ". Sorry!!!");
		}
	}
}

class ToPCI extends ToPCIHidden{
	public void auth(int refnum) {
		System.out.println(AuthMessageTransId+" Authorization Messages can be sent only to Acquirers and not to PCI Gateway!!!");
	}
	public void advice(int refnum) {
		System.out.println(AdviceMessageTransId+" Advice Messages can be sent only to Issuers and not to PCI Gateway!!!");
	}
	
}



public class AbIntTest {

	public static void main(String[] args) {
		
		ToIss iss = new ToIss();
		ToAcq acq = new ToAcq();
		ToPCI pci = new ToPCI();
		
		acq.auth(233);
		acq.auth(234);
		acq.advice(234);
		acq.clear(234);
		System.out.println();
		
		iss.advice(233);
		iss.advice(234);
		iss.auth(234);
		iss.clear(234);
		System.out.println();
		
		pci.clear(233);
		pci.clear(234);
		pci.auth(234);
		pci.advice(234);
		System.out.println();
		
	}
}
